---
layout: home
---

The UpdateManagement module is responsible for handling the mechanisms of an update loop to routinely update objects throughout the engine.
It provides the mechanism to update the state of objects through fixed time intervals and through updates with an elapsed time.