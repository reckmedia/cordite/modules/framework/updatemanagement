---
permalink: /usage/
layout: standard
title: Usage
nav_order: 2
---

## Setup

An `IUpdateManager` will need to be initialised and be made accessible for registering fixed updates (`Action`) and timed updates (`Action<TimeSpan>`) to.
The rate at which the updates will occur can either by defined by the `FixedUpdateRate` or `UpdatesPerSecond` properties.
Then this manager will need to have its `Update(TimeSpan elapsedTime)` method triggered for each intended game loop with an elapsed `TimeSpan` passed to it.

```c#
TimeSpan updateRate = TimeSpan.FromSeconds(0.016);
IUpdateManager updateManager = new UpdateManager{FixedUpdateRate = updateRate};

while(InfiniteWhileLoopsAreBad == true)
{
	TimeSpan elapsedTime = calculateElapsedTime();

	updateManager.Update(elapsedTime);

	Wait(updateRate);
}
```

### UpdateManager

The default implementation of the `IUpdateManager` interface, `UpdateManager`, needs to be provided with a fixed update rate that determines how often a fixed update is intended to occur.
This can be initialised either via a TimeSpan to specify the target duration between updates, or an int to specify the target number of updates per second.

```c#
IUpdateManager a = new UpdateManager(TimeSpan.FromSeconds(0.016));
IUpdateManager b = new UpdateManager(60);
```

## Fixed Updates

Objects intending to update at a fixed rate should implement a method without parameters and then register it to an implementation of `IUpdateManager`.
As the method has no parameter the update manager will treat it as a fixed update.
```c
class MyObject
{
	int iterations = 0;

	public void FixedUpdate()
	{
		iterations++;
	}
}

...
	IUpdateManager updateManager = new UpdateManager{UpdatesPerSecond = 60};
	MyObject myObject = new MyObject(1, 2, 3);
	updateManager.Register(myObject.FixedUpdate);
...
```

Its important to make sure that when disposing of an object it is unregistered from the update manager.

```c#
updateManager.Unregister(myObject.FixedUpdate);
myObject.Dispose();
```

## Timed Updates

Similar to the fixed update, objects that intend to update over elapsed time durations should implement a method which takes the elapsed time as a parameter then register it to an implementation of `IUpdateManager`:
As the method has a TimeSpan parameter the update manager will treat it as a timed update.
```c
class MyObject
{
	TimeSpan totalRunTime = TimeSpan.FromSeconds(0);

	public void TimedUpdate(TimeSpan elapsedTime)
	{
		totalRunTime += elapsedTime;
	}
}

...
	IUpdateManager updateManager = new UpdateManager{UpdatesPerSecond = 60};
	MyObject myObject = new MyObject(1, 2, 3);
	updateManager.Register(myObject.TimedUpdate);
...
```

Again, make sure to unregister it when disposing of the object.

```c#
updateManager.Unregister(myObject.TimedUpdate);
myObject.Dispose();
```

## Altering Update Speed

The speed of the updates can be altered through setting the `UpdateRateModifier`. This will alter the elapsed time passed into the system to simulate a faster or slower update.
For example setting the modifier to `2` would double the elapsed time value, causing timed updates to elapse twice as much time and fixed updates to trigger twice as often.
This can be useful for speeding up simulations, or for debugging.