---
permalink: /purpose/
layout: standard
title: Purpose
nav_order: 1
---

The UpdateManagement module is responsible for handling routine updates of various systems or objects in a game.
This can be done through either "Fixed" updates or "Timed" updates.

## Fixed Updates

Fixed updates may be triggered multiple times in one game loop or not at all based on the amount of elapsed time between game loops.
For example:
- An object needs to move 10 pixels per second.
- The game loop is set to 60 updates per second.
- The object is set to move 0.166 pixels per fixed update.
- The game loop runs after an elapsed time of 1 second, so the objects update is triggered 60 times moving it 10 pixels.
- The game loop runs after an elapsed time of 0.1 seconds, so the objects update is triggered 6 times moving it 1 pixel.
- The game loop runs after an elapsed time of 2 seconds, so the objects update is triggered 120 times moving it 20 pixels.

## Timed Updates

Timed updates will be triggered once per game loop.
They will be provided an elapsed time since the last call to update, to allow the method to handle interpolation of state between now, and the previous trigger.
For example:
- An object needs to move 10 pixels per second.
- The game loop is set to 60 updates per second.
- The object is set to move 10 pixels multiplied by the elapsed time in seconds.
- The game loop runs after an elapsed time of 1 second, so the objects update is triggered once moving it 10 (pixels) * 1 (second) = 10 pixels.
- The game loop runs after an elapsed time of 0.1 seconds, so the objects update is triggered once moving it 10 (pixels) * 0.1 (seconds) = 1 pixel.
- The game loop runs after an elapsed time of 2 seconds, so the objects update is triggered once moving it 10 (pixels) * 2 (seconds) = 20 pixels.


## UpdateManager

The update manager is responsible for triggering any registered fixed or timed updates at routine intervals.
This handles the mechanics of the fixed updates by calculating how many times they should be triggered if at all, and timed updates by triggering all of them once and passing the elapsed time to them.

The driving force of the update manager is not handled internally so will need to be triggered manually by an outside force for each intended game loop.