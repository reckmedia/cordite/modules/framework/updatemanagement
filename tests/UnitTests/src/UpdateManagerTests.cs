﻿using System;
using Cordite.Framework.UpdateManagement;
using UnitTests.Dummy;
using Xunit;

namespace UnitTests
{
	public class UpdateManagerTests
	{
		private FixedUpdatable _FixedUpdatable = new FixedUpdatable();

		[Fact]
		public void UpdatePerSecondIsCorrect()
		{
			UpdateManager updateManager = new UpdateManager
			{
				UpdatesPerSecond = 5
			};

			updateManager.Register(_FixedUpdatable.Update);

			updateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(5, _FixedUpdatable.TriggerCount);
		}

		[Fact]
		public void UpdateRateIsCorrect()
		{
			UpdateManager updateManager = new UpdateManager
			{
				FixedUpdateRate = TimeSpan.FromSeconds(0.1)
			};

			updateManager.Register(_FixedUpdatable.Update);

			updateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(10, _FixedUpdatable.TriggerCount);
		}
	}
}
