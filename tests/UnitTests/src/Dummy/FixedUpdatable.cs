﻿using System;

namespace UnitTests.Dummy
{
	internal class FixedUpdatable
	{
		public Int32 TriggerCount = 0;

		/// <inheritdoc />
		public void Update()
		{
			TriggerCount++;
		}
	}
}
