﻿using System;

namespace UnitTests.Dummy
{
	internal class TimedUpdatable
	{
		public Int32 TriggerCount = 0;

		public TimeSpan TotalElapsed = TimeSpan.Zero;

		public Double TestValue;

		/// <inheritdoc />
		public void Update(TimeSpan elapsedTime)
		{
			TotalElapsed += elapsedTime;

			TriggerCount +=1;

			TestValue += 5 * elapsedTime.TotalSeconds;
		}
	}
}
