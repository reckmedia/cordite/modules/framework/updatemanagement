﻿using System;
using Cordite.Framework.UpdateManagement;
using Cordite.Framework.UpdateManagement.Interface;
using UnitTests.Dummy;
using Xunit;

namespace UnitTests
{
	public class TimedUpdateTests
	{
		private readonly IUpdateManager _UpdateManager;
		private readonly TimedUpdatable _TimedUpdatable;

		public TimedUpdateTests()
		{
			_UpdateManager = new UpdateManager
			{
				FixedUpdateRate = TimeSpan.FromSeconds(1)
			};

			_TimedUpdatable = new TimedUpdatable();
			_UpdateManager.Register(_TimedUpdatable.Update);
		}

		[Fact]
		public void UpdatesForElapsedTime()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(5d, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(1), _TimedUpdatable.TotalElapsed);

			_UpdateManager.Update(TimeSpan.FromSeconds(0.5));

			Assert.Equal(7.5d, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(1.5), _TimedUpdatable.TotalElapsed);

			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(12.5d, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(2.5), _TimedUpdatable.TotalElapsed);
		}

		[Fact]
		public void TriggersOncePerUpdate()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _TimedUpdatable.TriggerCount);

			_UpdateManager.Update(TimeSpan.FromSeconds(0.5));

			Assert.Equal(2, _TimedUpdatable.TriggerCount);

			_UpdateManager.Update(TimeSpan.FromSeconds(5));

			Assert.Equal(3, _TimedUpdatable.TriggerCount);
		}

		[Fact]
		public void UnregisteredNoLongerUpdates()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _TimedUpdatable.TriggerCount);

			_UpdateManager.Unregister(_TimedUpdatable.Update);
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _TimedUpdatable.TriggerCount);
		}

		[Fact]
		public void HigherUpdateModifierSpeedsUp()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _TimedUpdatable.TriggerCount);
			Assert.Equal(5, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(1), _TimedUpdatable.TotalElapsed);

			_UpdateManager.UpdateRateModifier = 10d;
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(2, _TimedUpdatable.TriggerCount);
			Assert.Equal(55, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(11), _TimedUpdatable.TotalElapsed);
		}

		[Fact]
		public void LowerUpdateModifierSlowsDown()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _TimedUpdatable.TriggerCount);
			Assert.Equal(5, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(1), _TimedUpdatable.TotalElapsed);

			_UpdateManager.UpdateRateModifier = 0.1d;
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(2, _TimedUpdatable.TriggerCount);
			Assert.Equal(5.5, _TimedUpdatable.TestValue);
			Assert.Equal(TimeSpan.FromSeconds(1.1), _TimedUpdatable.TotalElapsed);
		}
	}
}
