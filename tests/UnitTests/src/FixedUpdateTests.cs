﻿using Cordite.Framework.UpdateManagement;
using Cordite.Framework.UpdateManagement.Interface;
using System;
using UnitTests.Dummy;
using Xunit;

namespace UnitTests
{
	public class FixedUpdateTests
	{
		private readonly IUpdateManager _UpdateManager;
		private readonly FixedUpdatable _FixedUpdatable;

		public FixedUpdateTests()
		{
			_UpdateManager = new UpdateManager
			{
				FixedUpdateRate = TimeSpan.FromSeconds(1)
			};

			_FixedUpdatable = new FixedUpdatable();
			_UpdateManager.Register(_FixedUpdatable.Update);
		}

		[Fact]
		public void CatchesUpOnMissedUpdates()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(10));

			Assert.Equal(10, _FixedUpdatable.TriggerCount);
		}

		[Fact]
		public void OnlyTriggersOnceEnoughTimeHasElapsed()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(0.5));

			Assert.Equal(0, _FixedUpdatable.TriggerCount);

			_UpdateManager.Update(TimeSpan.FromSeconds(0.5));

			Assert.Equal(1, _FixedUpdatable.TriggerCount);
		}

		[Fact]
		public void UnregisteredNoLongerUpdates()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _FixedUpdatable.TriggerCount);

			_UpdateManager.Unregister(_FixedUpdatable.Update);
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _FixedUpdatable.TriggerCount);
		}

		[Fact]
		public void HigherUpdateModifierSpeedsUp()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _FixedUpdatable.TriggerCount);

			_UpdateManager.UpdateRateModifier = 10d;
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(11, _FixedUpdatable.TriggerCount);
		}

		[Fact]
		public void LowerUpdateModifierSlowsDown()
		{
			_UpdateManager.Update(TimeSpan.FromSeconds(1));

			Assert.Equal(1, _FixedUpdatable.TriggerCount);

			_UpdateManager.UpdateRateModifier = 0.1d;
			_UpdateManager.Update(TimeSpan.FromSeconds(20));

			Assert.Equal(3, _FixedUpdatable.TriggerCount);
		}
	}
}