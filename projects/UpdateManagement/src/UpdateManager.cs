﻿using Cordite.Framework.UpdateManagement.Interface;
using System;
using System.Collections.Generic;

namespace Cordite.Framework.UpdateManagement
{
	/// <summary>
	/// The default implementation of the <see cref="IUpdateManager"/> that routinely triggers updates to registered objects.
	/// </summary>
	public class UpdateManager : IUpdateManager
	{
		private readonly List<Action> _FixedUpdaters;
		private readonly List<Action<TimeSpan>> _TimedUpdaters;

		private TimeSpan _FixedUpdateRate;
		private Double _UpdatesPerSecond;

		private TimeSpan _ModifiedElapsedTime;
		private Double _UnresolvedTicks;
		private Double _FixedIterations;

		/// <summary>
		/// Instantiates a new <see cref="UpdateManager"/>.
		/// </summary>
		public UpdateManager()
		{
			_FixedUpdaters = new List<Action>();
			_TimedUpdaters = new List<Action<TimeSpan>>();

			UpdatesPerSecond = 60;

			_UnresolvedTicks = 0;

			UpdateRateModifier = 1;
		}

		/// <summary>
		/// The intended rate for fixed updates to occur.
		/// </summary>
		public TimeSpan FixedUpdateRate
		{
			get => _FixedUpdateRate;
			set
			{
				_FixedUpdateRate = value;
				_UpdatesPerSecond = 1d / value.TotalSeconds;
			}
		}

		/// <summary>
		/// The number of times the update cycle should trigger per second.
		/// </summary>
		public Double UpdatesPerSecond
		{
			get => _UpdatesPerSecond;
			set => FixedUpdateRate = TimeSpan.FromSeconds(1d / value);
		}

		/// <inheritdoc />
		public Double UpdateRateModifier { get; set; }

		/// <inheritdoc />
		public void Register(Action fixedTick)
		{
			_FixedUpdaters.Add(fixedTick);
		}

		/// <inheritdoc />
		public void Register(Action<TimeSpan> timeTick)
		{
			_TimedUpdaters.Add(timeTick);
		}

		/// <inheritdoc />
		public void Unregister(Action fixedTick)
		{
			_FixedUpdaters.Remove(fixedTick);
		}

		/// <inheritdoc />
		public void Unregister(Action<TimeSpan> timeTick)
		{
			_TimedUpdaters.Remove(timeTick);
		}

		/// <inheritdoc />
		public void Update(TimeSpan elapsedTime)
		{
			_ModifiedElapsedTime = TimeSpan.FromMilliseconds(elapsedTime.TotalMilliseconds * UpdateRateModifier);

			for (Int32 i = 0; i < _TimedUpdaters.Count; i++)
			{
				Action<TimeSpan> o = _TimedUpdaters[i];
				o.Invoke(_ModifiedElapsedTime);
			}

			_UnresolvedTicks += _ModifiedElapsedTime.Ticks;
			_FixedIterations = _UnresolvedTicks / _FixedUpdateRate.Ticks;

			if (_FixedIterations < 1) return;

			_UnresolvedTicks %= _FixedUpdateRate.Ticks;

			for (Int32 iteration = 0; iteration < _FixedIterations; iteration++)
			{
				for (Int32 index = 0; index < _FixedUpdaters.Count; index++)
				{
					Action o = _FixedUpdaters[index];
					o.Invoke();
				}
			}
		}
	}
}
